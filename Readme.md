# Replication Package for the Paper on
## [How does Context Affect the Distribution of Software Maintainability Metrics?](http://feng-zhang.com/?p=669)

### Anything unclear, please don't hestitate to contact any of the authors.
--------------------------------------------------------------------------------

## 1. Authors

* [Feng Zhang](http://www.feng-zhang.com) (first name <at> cs.queensu.ca)
* [Audris Mockus](http://mockus.us) (first name <at> avaya.com)
* [Ying Zou](http://post.queensu.ca/~zouy) (first name <dot> last name <at> queensu.ca)
* [Foutse Khomh](http://www.khomh.net) (first name <dot> last name <at> polymtl.ca)
* [Ahmed E. Hassan](http://www.cs.queensu.ca/~ahmed/home) (first name <at> cs.queensu.ca)

## 2. Data Sources

Our empirical study uses SourceForge projects as our subject projects.
We have three types of data sources: cvs repositories by Audris Mockus, FLOSSmole data, and SourceForge.

### 2.1 CVS repositories are collected by Audris Mockus. 
As the whole dataset are 800GB and we don't have enough resouce to make them publically available, please contact any of the author to discuss how to transfer the whole dataset.

### 2.2 FLOSSmole data
FLOSSmole data can be found from [here](https://code.google.com/p/flossmole/downloads/list). In this dataset, we only use the topic data collected in June 2008, which can be downloaded from [here](https://code.google.com/p/flossmole/downloads/detail?name=sfRawTopicData2008-Jun.txt.bz2&can=2&q=sf).

### 2.3 SourceForge data

#### 2.3.1 Application domains
Application domains are downloaded directly from SourceForge.net. The URL format for each project is:

http://sourceforge.net/projects/ProjectName

where the ProjectName needs to be substituted by the real project name. For instance, [a2ixlibrary](http://sourceforge.net/projects/a2ixlibrary).

#### 2.3.2 Monthly downloads
Monthly downloads data are downloaded directly from SourceForget.net. The URL format for each project is:

http://sourceforge.net/projects/ProjectName/files/stats/json?start_date=1990-01-01&end_date=2012-12-31

where the ProjectName needs to be substituted by the real project name. For instance, [a2ixlibrary](http://sourceforge.net/projects/a2ixlibrary).

## 3. Categories of Subject Systems

The list of sampled 320 projects can be downloaded from [here](https://bitbucket.org/serap/contextstudy/src/b21cfd625179ea52c0d46b57b2779bdada31d440/listProjects?at=master).
The category information, of subject systems along six dimensions, can be downloaded from [here](https://bitbucket.org/serap/contextstudy/src/b21cfd625179ea52c0d46b57b2779bdada31d440/projectMultipleGrouping.csv?at=master).

## 4. Metrics Computation & Results

### 4.1 Metrics Computation
Our primary tool for computing metrics is a commercial tool called [Understand](http://www.scitools.com/documents/metricsList.php).
However, it cannot compute all metrics we want. Therefore, we wrote scripts by ourselves.
The scripts can be downloaded from [here](https://bitbucket.org/serap/contextstudy/src/b21cfd625179ea52c0d46b57b2779bdada31d440/scriptsForComputingMetrics.tar.gz?at=master).

### 4.2 Metrics Data
Due to size limitation on this server, we put the computed metrics (37.7MB) onto the [Bitbucket](https://bitbucket.org) server.
Our project on BitBucket is [ContextStudy](https://bitbucket.org/serap/contextstudy), which contains the metric data.
The data also can be downloaded directly from [this link](https://bitbucket.org/serap/contextstudy/src/b21cfd625179ea52c0d46b57b2779bdada31d440/metricsData.tar.gz?at=master).

## 5. R Script & Output

### 5.1 R Script
The R script for analyzing the impact of contextual factors on the distribution of the values of software maintainability metrics, can be downloaded from [here](https://bitbucket.org/serap/contextstudy/src/b21cfd625179ea52c0d46b57b2779bdada31d440/rscripts.tar.gz?at=master).

### 5.2 R Output
The results, of R script for processing subject software systems, can be downloaded from [here](https://bitbucket.org/serap/contextstudy/src/b21cfd625179ea52c0d46b57b2779bdada31d440/routput_2013_04_24.tar.gz?at=master).

## 6. Reference

Anyone is welcome to use this dataset. If you use our data in your work, please consider to cite our paper:

### 6.1 Plain Text
> Feng Zhang, Audris Mockus, Ying Zou, Foutse Khomh, and Ahmed E. Hassan, **How does Context affect the Distribution of Software Maintainability Metrics?**, Proceedings of the 29th IEEE International Conference on Software Maintainability (ICSM'13), September 22-28, 2013, Eindhoven, The Netherlands.

### 6.2 BibTex Entry
    @inproceedings{zmzkh-icsm-2013,
    author = {Zhang, Feng and Mockus, Audris and Zou, Ying and Khomh, Foutse and Hassan, Ahmed E.},
    title = {How does Context affect the Distribution of Software Maintainability Metrics?},
    booktitle = {Proceedings of the 29th IEEE International Conference on Software Maintainability},
    series = {ICSM '13},
    year = {2013},
    numpages = {10},
    keywords = {context, contextual factor, metrics, static metrics, software maintainability, benchmark, sampling, mining software repositories, large scale},
    }
